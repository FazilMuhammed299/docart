import React, {memo} from "react";
import {TouchableOpacity, View, Text, Image} from 'react-native';
import { Colors } from "../config";
import Icons from 'react-native-vector-icons/Ionicons'
const Header = ({onePress, back, title}) => {

return (

<View style={{ backgroundColor: Colors.primaryColor, height: 50}}>
    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 5}}>

        <View style={{ flexDirection: 'row', alignItems: 'center'}}>
            {
                back ?  <TouchableOpacity onPress={ onePress}>
                <Icons name="arrow-back" style={{color: '#fff'}} size={30} />
                </TouchableOpacity> :  <TouchableOpacity onPress={onePress}>
    <Icons name="ios-menu" style={{color: '#fff'}} size={40} />
    </TouchableOpacity>
            }
       
       {
        back ? <Text style={{ color: '#fff', fontSize: 18, fontWeight: '600', marginLeft: 10}}>{title}</Text> : 
       

    <Image 
                 source={require('../assets/LOGO.png')} 
                 resizeMethod="auto" 
                 resizeMode='contain' 
                 style={{ width: 70, height: 50, marginLeft: 10}} />
       }
        </View>
    
                 {!back &&  <Icons name="cart" style={{ color: '#fff' }} size={25} />}
    </View>
   
    
</View>
)

}

export default memo(Header)