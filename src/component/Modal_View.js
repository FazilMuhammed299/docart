/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect, useContext} from 'react';
import {View, Text, TouchableOpacity, Image, StyleSheet} from 'react-native';
import {UserContext} from '../store/UserContext';
import Modal from 'react-native-modal';
const DailyModal = ({
  isModalVisible = false,
  CloseisModalVisible,
  onPressContinueAction,
  onPressLaterAction,
  initialseconds = 5,
  building,
  road,
  village,
  state_district,
  state,
  country,
}) => {
  const {locationDetails, latitude, longitude} = useContext(UserContext);
  const [seconds, setSeconds] = useState(initialseconds);
  useEffect(() => {
    setSeconds(initialseconds);
  }, [isModalVisible]);
  useEffect(() => {
    if (isModalVisible) {
      let myInterval = setInterval(() => {
        if (seconds > 0) {
          setSeconds(seconds - 1);
        }
        if (seconds === 0) {
          setSeconds(seconds);
        }
      }, 1000);
      return () => {
        clearInterval(myInterval);
      };
    }
  }, [isModalVisible, seconds]);
  return (
    <Modal
      onBackdropPress={() => CloseisModalVisible()}
      style={{
        margin: 0,
        backgroundColor: 'rgba(0,0,0,0.6)',
        padding: 10,
      }}
      backdropColor={'black'}
      backdropOpacity={0.8}
      visible={isModalVisible}
      dismiss={() => CloseisModalVisible()}>
      <View
        style={{
          marginVertical: '45%',
          borderRadius: 15,
          marginHorizontal: 10,
          backgroundColor: '#fff',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <View style={{}}>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                color: 'black',
                fontSize: 20,
                fontWeight: 'bold',
                paddingVertical: 10,
                alignItems: 'center',
                justifyContent: 'center',
                textAlign: 'center',
              }}>
              Save Your Location
            </Text>

            {building && (
              <Text
                style={{
                  color: 'grey',
                  alignItems: 'center',
                  justifyContent: 'center',
                  // fontWeight: 'bold',
                  fontSize: 12,
                }}>
                {building}
              </Text>
            )}
            {road && (
              <Text
                style={{
                  color: 'grey',
                  // fontSize: 18,
                  fontWeight: 'bold',
                  // paddingVertical: 10,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {road}
              </Text>
            )}
            {village && (
              <Text
                style={{
                  color: 'grey',
                  fontSize: 13,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {village}
              </Text>
            )}

            {state_district && (
              <Text
                style={{
                  color: 'grey',
                  fontSize: 13,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {state_district}
              </Text>
            )}

            {state && (
              <Text
                style={{
                  color: 'grey',
                  fontSize: 13,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {state}
              </Text>
            )}
            {country && (
              <Text
                style={{
                  color: 'grey',
                  fontSize: 13,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {country}
              </Text>
            )}
            {latitude && (
              <Text
                style={{
                  color: 'grey',
                  fontSize: 13,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                Latitude :{latitude}
              </Text>
            )}
            {longitude && (
              <Text
                style={{
                  fontSize: 13,
                  color: 'grey',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                Longitude :{longitude}
              </Text>
            )}
          </View>
          <TouchableOpacity
            onPress={() => onPressContinueAction()}
            style={{
              backgroundColor: '#2BB27E',
              marginHorizontal: 10,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 10,
              height: 35,
              marginVertical: 30,
            }}>
            <Text
              style={{
                color: '#fff',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              Save Location
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};
export default DailyModal;

const styles = StyleSheet.create({
  textstyle: {
    color: '#000',
  },
});
