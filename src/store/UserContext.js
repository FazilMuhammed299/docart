import React, {useReducer, createContext, useMemo, useEffect} from 'react';
import {Platform} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {Snackbar} from 'react-native-paper';
import {initialState, authReducer, CONSTANT} from './UserReducer';

import * as NavigationRef from '../navigation/NavigationRef';

const storeData = async value => {
  try {
    const jsonValue = JSON.stringify(value);
    await AsyncStorage.setItem('@userData', jsonValue);
  } catch (e) {
    // saving error
  }
};
export const UserContext = createContext();

export const UserProvider = props => {
  const [state, dispatch] = useReducer(authReducer, initialState);

  const [visible, setVisible] = React.useState(false);
  const [message, setMessage] = React.useState('');

  const showToast = msg => {
    setVisible(!visible);
    setMessage(msg);
  };

  const onDismissSnackBar = () => setVisible(false);

  const userContext = useMemo(
    () => ({
      getData: url => {
        return new Promise((resolve, reject) => {
          axios
            .post(url, {
              timeout: 10000,
              headers: {
                Accept: 'application/json',
              },
            })
            .then(res => {
              if (res) {
                let response = res.data;
                resolve(response);
              } else {
                showToast('Something went wrong');
              }
            })
            .catch(error => {
              showToast('Check your internet connection');
              resolve(null);
              reject(error);
            });
        });
      },

      cartItemAdd: (item) => {
        dispatch({type: CONSTANT.add_cart, payload: item})
        showToast("Added Item to cart")
      },

      removeItem: (item) => {
   

        let array = state.cartItems
        let resultArray = array.filter((value,index) =>  value != item)
    
        dispatch({type: CONSTANT.removeItem, data: resultArray})
      }

     
    }),

    [state],
  );

  useEffect(() => {
    // Fetch the token from storage then navigate to our appropriate place
    const initialize = async () => {
      let userData;

      try {
        userData = await AsyncStorage.getItem('@userData');
      } catch (e) {}

      dispatch({
        type: 'RESTORE_TOKEN',
        state: userData ? JSON.parse(userData) : {},
      });
    };
    if (state.isLoading) {
      initialize();
    } else {
      storeData(state);
    }
  }, [state]);

  return (
    <UserContext.Provider value={{...userContext, ...state, showToast}}>
      {props.children}
      <Snackbar
        visible={visible}
        duration={3000}
        style={{borderRadius: 10}}
        onDismiss={onDismissSnackBar}>
        {message}
      </Snackbar>
    </UserContext.Provider>
  );
};
