export const CONSTANT = {
  restore_data: 'RESTORE_TOKEN',
  add_cart: 'ADD_CART',
  removeItem: 'REMOVE_ITEM'
};

export const initialState = {
  isLoading: true,
  cartItems: []
 };

export const authReducer = (state, action) => {
  switch (action.type) {
    case CONSTANT.restore_data:
      return {
        ...state,
        ...action.state,
        isLoading: false,
      };
      case CONSTANT.add_cart:


        return {
          ...state,
          cartItems: [...state.cartItems, action.payload]
        }
case CONSTANT.removeItem: 



return {
  ...state,
  cartItems: action.data
}


    default:
      return state;
  }
};

export default authReducer;
