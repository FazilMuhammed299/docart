export const ENDPOINT = 'https://demo9.iossmlm.com/jabir_ind_live';


export const Categories = [
    {
        id: 0,
        name: 'All Products',
        image: 'https://www.jaindairy.com/wp-content/uploads/2022/05/cheese-sauce_Jalpeno.jpg'
    }, {
        id: 1,
        name: 'Egg, Fish & Meet',
        image: 'https://img.freepik.com/premium-photo/egg-chicken-isolated-white-background_158502-176.jpg?w=1380'
    },
    {
        id: 2,
        name: 'Ice Cream,Milk & Milk Products',
        image: 'https://img.freepik.com/free-vector/vanilla-ice-cream-scoop-closeup-realistic-composition-with-aromatic-dried-beans-fresh-yellow-flower_1284-31943.jpg?w=1060&t=st=1670402068~exp=1670402668~hmac=08e7c60e5092ac958e2d3815ba0900c21d97f51ad328946373e934a680b739d2'
    },
    {
        id: 3,
        name: 'Baby Care & Accesories',
        image: 'https://cdn-icons-png.flaticon.com/512/1012/1012587.png?w=1060&t=st=1670402206~exp=1670402806~hmac=fbe5051c9723f14669290e7755c583aae1eefe480f3fbe18e8bd116d0c03dc2d'
    }, {
        id: 4,
        name: 'Cleaning and Household items',
        image: 'https://img.freepik.com/free-photo/white-soap-bars-isolated-white-background_93675-135295.jpg?w=1800&t=st=1670402347~exp=1670402947~hmac=1841c4aaa44992285f951f36971fe3c93452af7966cd03cfdadb5f4096575b17'
    },
    {
        id: 5,
        name: 'Egg, Fish & Meet',
        image: 'https://www.jaindairy.com/wp-content/uploads/2022/05/cheese-sauce_Jalpeno.jpg'
    },
    {
        id: 6,
        name: 'Baby Care & Accesories',
        image: 'https://www.jaindairy.com/wp-content/uploads/2022/05/cheese-sauce_Jalpeno.jpg'
    }, {
        id: 7,
        name: 'Cleaning and Household items',
        image: 'https://img.freepik.com/premium-photo/egg-chicken-isolated-white-background_158502-176.jpg?w=1380'
    },
    {
        id: 8,
        name: 'Egg, Fish & Meet',
        image: 'https://www.jaindairy.com/wp-content/uploads/2022/05/cheese-sauce_Jalpeno.jpg'
    }
]



export const Products = [
    {
        id: 0,
        name: 'Soap',
        price: '100',
        revard: '4.4',
        bv: '0.75',
        image: 'https://www.jaindairy.com/wp-content/uploads/2022/05/cheese-sauce_Jalpeno.jpg'
    }, {
        id: 1,
        name: 'Brush',
        price: '200.00',
        revard: '4.4',
        bv: '0.75',
        image: 'https://img.freepik.com/premium-photo/egg-chicken-isolated-white-background_158502-176.jpg?w=1380'
    },
    {
        id: 2,
        name: 'Ice Cream',
        price: '10.00',
        revard: '4.4',
        bv: '0.75',
        image: 'https://img.freepik.com/free-vector/vanilla-ice-cream-scoop-closeup-realistic-composition-with-aromatic-dried-beans-fresh-yellow-flower_1284-31943.jpg?w=1060&t=st=1670402068~exp=1670402668~hmac=08e7c60e5092ac958e2d3815ba0900c21d97f51ad328946373e934a680b739d2'
    },
    {
        id: 3,
        name: 'Oil',
        price: '50.00',
        revard: '4.4',
        bv: '0.75',
        image: 'https://cdn-icons-png.flaticon.com/512/1012/1012587.png?w=1060&t=st=1670402206~exp=1670402806~hmac=fbe5051c9723f14669290e7755c583aae1eefe480f3fbe18e8bd116d0c03dc2d'
    }, {
        id: 4,
        name: 'Sugar',
        price: '70.00',
        revard: '4.4',
        bv: '0.75',
        image: 'https://img.freepik.com/free-photo/white-soap-bars-isolated-white-background_93675-135295.jpg?w=1800&t=st=1670402347~exp=1670402947~hmac=1841c4aaa44992285f951f36971fe3c93452af7966cd03cfdadb5f4096575b17'
    },
    {
        id: 5,
        name: 'Salt',
        price: '20.00',
        revard: '4.4',
        bv: '0.75',
        image: 'https://www.jaindairy.com/wp-content/uploads/2022/05/cheese-sauce_Jalpeno.jpg'
    },
    {
        id: 6,
        name: 'Shamboo',
        price: '120.00',
        revard: '4.4',
        bv: '0.75',
        image: 'https://www.jaindairy.com/wp-content/uploads/2022/05/cheese-sauce_Jalpeno.jpg'
    }, {
        id: 7,
        name: 'Bucket',
        price: '70.00',
        revard: '4.4',
        bv: '0.75',
        image: 'https://img.freepik.com/premium-photo/egg-chicken-isolated-white-background_158502-176.jpg?w=1380'
    },
    {
        id: 8,
        name: 'Egg',
        price: '5.00',
        revard: '4.4',
        bv: '0.75',
        image: 'https://www.jaindairy.com/wp-content/uploads/2022/05/cheese-sauce_Jalpeno.jpg'
    }
]




export const MyOrders = [
    {
        id: 0,
        name: 'Soap',
        price: '100',
        revard: '4.4',
        bv: '0.75',
        image: 'https://www.jaindairy.com/wp-content/uploads/2022/05/cheese-sauce_Jalpeno.jpg',
        status: 'Pending',
        OrderID: '#91922'
    }, {
        id: 1,
        name: 'Brush',
        price: '200.00',
        revard: '4.4',
        bv: '0.75',
        image: 'https://img.freepik.com/premium-photo/egg-chicken-isolated-white-background_158502-176.jpg?w=1380',
        status: 'Shipped',
        OrderID: '#91922'
    },
    {
        id: 2,
        name: 'Ice Cream',
        price: '10.00',
        revard: '4.4',
        bv: '0.75',
        image: 'https://img.freepik.com/free-vector/vanilla-ice-cream-scoop-closeup-realistic-composition-with-aromatic-dried-beans-fresh-yellow-flower_1284-31943.jpg?w=1060&t=st=1670402068~exp=1670402668~hmac=08e7c60e5092ac958e2d3815ba0900c21d97f51ad328946373e934a680b739d2',
        status: 'Pending',
        OrderID: '#91922'
    },
    {
        id: 3,
        name: 'Oil',
        price: '50.00',
        revard: '4.4',
        bv: '0.75',
        image: 'https://cdn-icons-png.flaticon.com/512/1012/1012587.png?w=1060&t=st=1670402206~exp=1670402806~hmac=fbe5051c9723f14669290e7755c583aae1eefe480f3fbe18e8bd116d0c03dc2d',
        status: 'Shipped',
        OrderID: '#91922'
    }, {
        id: 4,
        name: 'Sugar',
        price: '70.00',
        revard: '4.4',
        bv: '0.75',
        image: 'https://img.freepik.com/free-photo/white-soap-bars-isolated-white-background_93675-135295.jpg?w=1800&t=st=1670402347~exp=1670402947~hmac=1841c4aaa44992285f951f36971fe3c93452af7966cd03cfdadb5f4096575b17',
        status: 'Canceled',
        OrderID: '#91922'
    },
    
]
