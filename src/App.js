/* eslint-disable react-native/no-inline-styles */
import React, {useEffect} from 'react';

// import {SafeAreaView} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import Nav from './navigation';

import {UserProvider} from './store/UserContext';
import {SafeAreaProvider, SafeAreaView} from 'react-native-safe-area-context';
import { Colors } from './config';

const MyApp = () => {


  return (
    <NavigationContainer>
      <SafeAreaProvider>
        <UserProvider>
             <SafeAreaView style={{flex: 1, backgroundColor: Colors.primaryColor }}>
                <Nav />
             </SafeAreaView>
        </UserProvider>
      </SafeAreaProvider>
    </NavigationContainer>
  );
};
export default MyApp;
