/* eslint-disable react-native/no-inline-styles */
import React, {useContext} from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
// import { createDrawerNavigator } from '@react-navigation/drawer';
import {Text, View, Platform, Image, ActivityIndicator,   useWindowDimensions, StyleSheet} from 'react-native';
import {StatusBar} from 'react-native';
import CustomSidebarMenu from './CustomDrawer';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';

// Auth Screens
import AuthLoading from '../screens/AuthLoading';
import Authentication from '../screens/Authetication';


//tabscreens of user
import HomeScreen from '../screens/Home';
import MyOrder from '../screens/MyOrder';
import MyCart from '../screens/MyCart';
import ProdctDetails from '../screens/ProductDetails';

import Category from '../screens/Category';
import {Colors} from '../config';
import Icon from 'react-native-vector-icons/Ionicons';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const Drawer = createDrawerNavigator();



const HomeStack = () => {
  return (
    <Stack.Navigator
    
      screenOptions={{
        headerShown: false,
        statusBarStyle: 'dark',
      }}>
      <Stack.Screen name="HomeScreen" component={HomeScreen} />
      <Stack.Screen name="Category" component={Category} />
    </Stack.Navigator>
  );
}


const OrderStatck = () => {
  return (
    <Stack.Navigator
    
      screenOptions={{
        headerShown: false,
        statusBarStyle: 'dark',
      }}>
      <Stack.Screen name="MyOrders" component={MyOrder} />
      <Stack.Screen name="OrderDetails" component={ProdctDetails} />
    </Stack.Navigator>
  );
}






const DrawerNavigation = () => {
  return (
 
      <Drawer.Navigator initialRouteName="Home"  
      // drawerContent={(props) => <CustomDrawerContent {...props} />}>

drawerContent={(props) => <CustomSidebarMenu {...props} />}>
        <Drawer.Screen name="Home" component={HomeStack} options={{
          headerShown: false,
          title: 'Home',
          drawerIcon: ({focused, size}) => (
            <Icon
              name="home"
              type="font-awesome"
              size={size}
              color={focused ? Colors.primaryColor : '#ccc'}
            />
          )
        }} />
        <Drawer.Screen name="MyOrder" component={OrderStatck} 
        options={{
          headerShown: false,
          title: 'My Orders',
          drawerIcon: ({focused, size}) => (
            <Icon
              name="grid-outline"
              type="font-awesome"
              size={size}
              color={focused ? Colors.primaryColor : '#ccc'}
            />
          )
        }} 
        />

<Drawer.Screen name="MyCart" component={MyCart} 
        options={{
          headerShown: false,
          title: 'My Cart',
          drawerIcon: ({focused, size}) => (
            <Icon
              name="cart"
              type="font-awesome"
              size={size}
              color={focused ? Colors.primaryColor : '#ccc'}
            />
          )
        }} 
        />

      </Drawer.Navigator>

  );
}

function Nav() {
  return (
    <Stack.Navigator
    
      screenOptions={{
        headerShown: false,
        statusBarStyle: 'dark',
      }}>
      <Stack.Screen name="AuthLoading" component={AuthLoading} />
      <Stack.Screen name="Authentication" component={Authentication} />
      <Stack.Screen name="MainDrawer" component={DrawerNavigation} />
    </Stack.Navigator>
  );
}
export default Nav;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  menuContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  menuItemsCard: {
    // flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  circleContainer: {
    width: 50,
    height: 50,
    borderRadius: 25,
    padding: 10,
  },
});