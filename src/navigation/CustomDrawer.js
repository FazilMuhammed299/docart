// Custom Navigation Drawer / Sidebar with Image and Icon in Menu Options
// https://aboutreact.com/custom-navigation-drawer-sidebar-with-image-and-icon-in-menu-options/

import React from 'react';
import {
  SafeAreaView,
  View,
  StyleSheet,
  Image,
  Text,
  Linking,
} from 'react-native';

import {
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import { Colors } from '../config';
import Icons from 'react-native-vector-icons/Ionicons'
const CustomSidebarMenu = (props) => {


  return (
    <SafeAreaView style={{flex: 1}}>
     
      <View style={{ backgroundColor: Colors.primaryColor, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 10}}>
        <View style={{ flexDirection: 'row', alignItems: 'center'}}>
<Icons name="person-circle-outline" size={30} style={{color: '#fff'}}/>
        <Text style={{ color: '#fff', fontWeight: '700', paddingVertical: 10, paddingLeft: 10}}>Hello jabir</Text>
        </View>
        <Image 
                 source={require('../assets/LOGO.png')} 
                 resizeMethod="auto" 
                 resizeMode='contain' 
                 style={{ width: 70, height: 50, marginLeft: 10}} />
      </View>
      <DrawerContentScrollView {...props}>
        <DrawerItemList {...props} />
       
      </DrawerContentScrollView>
  
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sideMenuProfileIcon: {
    resizeMode: 'center',
    width: 100,
    height: 100,
    borderRadius: 100 / 2,
    alignSelf: 'center',
  },
  iconStyle: {
    width: 15,
    height: 15,
    marginHorizontal: 5,
  },
  customItem: {
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default CustomSidebarMenu;