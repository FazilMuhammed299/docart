/* eslint-disable react-native/no-inline-styles */
import React, {
  useContext,
  useEffect,
  useState,
  useCallback,
  useRef,
} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TextInput,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {UserContext} from '../../store/UserContext';
import {ActivityIndicator} from 'react-native-paper';
import {Categories} from '../../store/Endpoint';
import Header from '../../component/Header';
import {Colors} from '../../config';
const {width, height} = Dimensions.get('window');
import Icons from 'react-native-vector-icons/Ionicons';

const HomeScreen = ({navigation}) => {
  const [searchText, setSearchText] = useState('');

  const _renderItem = ({item}) => {
    return (
      <TouchableOpacity
        onPress={() => navigation.navigate('Category')}
        style={{
          backgroundColor: '#fff',
          margin: 2,
          height: 130,
          justifyContent: 'center',
          alignItems: 'center',
          width: 120,
          elevation: 3,
          shadowColor: '#515151',
          shadowOffset: {width: 0, height: 1},
          shadowOpacity: 0.2,
          shadowRadius: 2,
          borderRadius: 10,
        }}>
        <Image
          source={{uri: item.image}}
          style={{width: 100, height: 90}}
          resizeMode="contain"
        />
        <Text style={{textAlign: 'center'}}>{item.name}</Text>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <Header onePress={() => navigation.toggleDrawer()} />

      <View
        style={{
          backgroundColor: Colors.primaryColor,
          paddingHorizontal: 20,
          paddingVertical: 10,
        }}>
        <View
          style={{
            height: 45,
            width: '100%',
            backgroundColor: 'white',
            borderRadius: 10,
            justifyContent: 'center',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingHorizontal: 10,
          }}>
          <TextInput
            value={searchText}
            onChangeText={text => setSearchText(text)}
            placeholder="Search products brand and more"
          />
          <Icons name="search" size={25} />
        </View>
      </View>

      <Text
        style={{
          padding: 10,
          fontSize: 22,
          color: 'steelblue',
          fontWeight: '600',
        }}>
        Categories
      </Text>

      <FlatList
        numColumns={Categories.length / 3} // set number of columns
        columnWrapperStyle={{
          justifyContent: 'space-around',
          marginHorizontal: 10,
        }}
        data={Categories}
        keyExtractor={(item, index) => index.toString()}
        renderItem={_renderItem}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#f4f4f4',
  },
});

export default HomeScreen;
