/* eslint-disable react-native/no-inline-styles */
import React, {
  useContext,
  useEffect,
  useState,
  useCallback,
  useRef,
} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {UserContext} from '../../store/UserContext';
import {ActivityIndicator} from 'react-native-paper';
import Header from '../../component/Header';
import {Products} from '../../store/Endpoint';
const {width, height} = Dimensions.get('window');

const Category = ({navigation}) => {
  const {cartItemAdd} = useContext(UserContext);

  const _renderItem = ({item}) => {
    return (
      <TouchableOpacity
        style={{
          backgroundColor: '#fff',
          margin: 2,
          height: 230,
          width: 180,
          elevation: 3,
          shadowColor: '#515151',
          shadowOffset: {width: 0, height: 1},
          shadowOpacity: 0.2,
          shadowRadius: 2,
          borderRadius: 5,
        }}>
        <Image
          source={{uri: item.image}}
          style={{width: 100, height: 90, alignSelf: 'center'}}
          resizeMode="contain"
        />
        <Text style={{textAlign: 'center'}}>{item.name}</Text>
        <View style={{paddingLeft: 10, paddingTop: 5}}>
          <Text style={{textAlign: 'left', color: 'red', fontSize: 16}}>
            ₹ {item.price}
          </Text>
          <Text
            style={{
              textAlign: 'left',
              fontSize: 12,
              color: '#aaa',
              paddingVertical: 10,
            }}>
            Revard point {item.revard}
          </Text>
          <Text style={{textAlign: 'left', fontSize: 12, color: '#aaa'}}>
            bv {item.bv}
          </Text>
          <TouchableOpacity
            onPress={() => cartItemAdd(item)}
            style={{
              backgroundColor: 'steelblue',
              height: 30,
              width: 70,
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 20,
              marginTop: 10,
            }}>
            <Text style={{color: '#fff'}}>Add</Text>
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <Header back title="All Products" onePress={() => navigation.goBack()} />

      <FlatList
        style={{paddingTop: 10}}
        numColumns={2} // set number of columns
        columnWrapperStyle={{
          justifyContent: 'space-around',
          marginHorizontal: 10,
        }}
        data={Products}
        keyExtractor={(item, index) => index.toString()}
        renderItem={_renderItem}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f4f4f4',
  },
});

export default Category;
