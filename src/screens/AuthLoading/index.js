import React, {useContext, useEffect, useState, useRef} from 'react';
import {View, StyleSheet, Image} from 'react-native';
import {ActivityIndicator} from 'react-native-paper';
import {Colors} from '../../config';
import {UserContext} from '../../store/UserContext';

let subscription;

const AuthLoading = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('Authentication');
    }, 200);
  }, []);

  return (
    <View style={[styles.container]}>
      <Image
        source={require('../../assets/LOGO.png')}
        resizeMethod="auto"
        resizeMode="contain"
        style={{width: 150, height: 150}}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f4f4f4',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.primaryColor,
  },
});

export default AuthLoading;
