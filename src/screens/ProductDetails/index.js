import React from 'react';
import {View, Text} from 'react-native';
import Header from '../../component/Header';
import {Colors} from '../../config';

const ProductDetails = ({navigation, route}) => {
  let {itemData} = route.params;
  return (
    <View style={{flex: 1}}>
      <Header
        back
        title={`My Order ${itemData.OrderID}`}
        Order
        onePress={() => navigation.goBack()}
      />

      <View style={{padding: 10}}>
        <Text style={{fontSize: 18, color: '#444444'}}>
          Order ID : {itemData.OrderID}
        </Text>
        <Text style={{fontSize: 18, color: '#444444', paddingVertical: 5}}>
          Ordered : Dec 29 2020
        </Text>
        <Text style={{fontSize: 18, color: '#444444'}}>Customer : Admin</Text>
        <Text style={{fontSize: 18, color: '#444444', paddingVertical: 5}}>
          Payment : Cash ON Delivery
        </Text>
      </View>
      <View style={{paddingHorizontal: 10}}>
        <Text style={{fontSize: 18}}>{itemData.name}</Text>
        <Text style={{fontSize: 16, paddingVertical: 5, color: '#aaa'}}>
          Quantity 500 g
        </Text>
        <Text style={{fontSize: 18, color: Colors.primaryred}}>
          ₹ {itemData.price}
        </Text>
      </View>
    </View>
  );
};

export default ProductDetails;
