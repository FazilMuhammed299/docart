/* eslint-disable react-native/no-inline-styles */
import React, {
  useContext,
  useEffect,
  useState,
  useCallback,
  useRef,
} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {UserContext} from '../../store/UserContext';
import {ActivityIndicator} from 'react-native-paper';
import Header from '../../component/Header';
import {Colors} from '../../config';
import DefaultText from '../../component/DefaultText';
const {width, height} = Dimensions.get('window');
import {MyOrders} from '../../store/Endpoint';
import Icons from 'react-native-vector-icons/Ionicons';
const itemWidth = (width - 15) / 2;

const column = 2;
const margin = 10;
const SIZE = (width - margin * column * 2) / column;

const MyOrder = ({navigation}) => {
  const {cartItems, removeItem} = useContext(UserContext);

  const _renderEmpty = () => {
    return (
      <View
        style={{
          flex: 1,
          height: 300,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text>No Items</Text>
      </View>
    );
  };

  const _renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => navigation.navigate('OrderDetails', {itemData: item})}
        style={{
          backgroundColor: '#fff',
          margin: 2,
          height: 120,
          paddingHorizontal: 15,
          padding: 10,
          elevation: 3,
          shadowColor: '#515151',
          shadowOffset: {width: 0, height: 1},
          shadowOpacity: 0.2,
          shadowRadius: 2,
          borderRadius: 5,
        }}>
        <DefaultText style={{}}>
          Order ID <Text style={{fontWeight: '500'}}>{item.OrderID}</Text>
        </DefaultText>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text>Status</Text>
          <View
            style={{
              backgroundColor:
                item.status == 'Pending'
                  ? Colors.primaryColor
                  : item.status == 'Shipped'
                  ? 'green'
                  : 'red',
              width: 70,
              marginLeft: 10,
              alignItems: 'center',
              justifyContent: 'center',
              height: 30,
              borderRadius: 10,
              margin: 10,
            }}>
            <Text style={{color: 'white'}}>{item.status}</Text>
          </View>
        </View>

        <DefaultText>
          Items : <Text style={{fontWeight: '700'}}>{item.name}</Text>
        </DefaultText>

        <View style={{position: 'absolute', right: 10, top: 10}}>
          <Icons
            name="chevron-forward-outline"
            size={20}
            style={{color: '#aaa'}}
          />
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <Header onePress={() => navigation.toggleDrawer()} />
      <FlatList
        style={{paddingTop: 10}}
        ListEmptyComponent={_renderEmpty}
        data={MyOrders}
        keyExtractor={(item, index) => index.toString()}
        renderItem={_renderItem}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f4f4f4',
  },
});

export default MyOrder;
