import React, {useState, useContext} from 'react';
import {View, Text, TouchableOpacity, TextInput, Image} from 'react-native';
import axios from 'axios';
import {Colors} from '../../config';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {ENDPOINT} from '../../store/Endpoint';
import {UserContext} from '../../store/UserContext';
import DefaultText from '../../component/DefaultText';
const Authentication = ({navigation}) => {
  const [username, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const {showToast} = useContext(UserContext);

  const loginAction = () => {
    if (username.length == 0) {
      showToast('Enter your email');
      return;
    } else if (password.length == 0) {
      showToast('Enter your password');
      return;
    }

    let data = {
      email: username,
      password: password,
    };

    axios
      .post(`${ENDPOINT}/store/index.php?route=api/login/`, data)
      .then(res => {
        console.log(res, 'responsee');
        navigation.navigate('MainDrawer');
      })
      .catch(err => {
        navigation.navigate('MainDrawer');
      });
  };

  return (
    <View style={{flex: 1, backgroundColor: Colors.primaryColor}}>
      <KeyboardAwareScrollView
        contentContainerStyle={{flexGrow: 1, justifyContent: 'center'}}>
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <Image
            source={require('../../assets/LOGO.png')}
            resizeMethod="auto"
            resizeMode="contain"
            style={{width: 150, height: 150}}
          />
        </View>

        <View style={{marginHorizontal: 20}}>
          <View
            style={{backgroundColor: '#fff', borderRadius: 20, padding: 20}}>
            <TextInput
              value={username}
              onChangeText={text => setUserName(text)}
              placeholder="Username / Mobile Number"
              style={{height: 45}}
            />
            <View style={{height: 2, backgroundColor: '#ddd'}} />
            <TextInput
              value={password}
              secureTextEntry={true}
              onChangeText={text => setPassword(text)}
              placeholder="Password"
              style={{height: 45}}
            />
          </View>

          <TouchableOpacity activeOpacity={0.8}>
            <Text
              style={{
                color: '#fff',
                paddingVertical: 10,
                textAlign: 'right',
                fontWeight: '500',
              }}>
              Forgot Password ?
            </Text>
          </TouchableOpacity>
        </View>

        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginVertical: 20,
          }}>
          <TouchableOpacity
            onPress={() => loginAction()}
            style={{
              backgroundColor: Colors.buttonColor,
              paddingHorizontal: 50,
              paddingVertical: 10,
              borderRadius: 20,
            }}>
            <Text style={{color: '#fff'}}>User Login</Text>
          </TouchableOpacity>
        </View>

        <DefaultText
          style={{textAlign: 'center', color: 'white', fontWeight: '600'}}>
          Don't have an Account ?{' '}
          <Text style={{color: Colors.buttonColor}}>Signup now</Text>{' '}
        </DefaultText>
      </KeyboardAwareScrollView>
    </View>
  );
};

export default Authentication;
